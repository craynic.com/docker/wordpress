#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d/"

ENVS=(
  WORDPRESS_URL
  WORDPRESS_TITLE
  WORDPRESS_ADMIN_NAME
  WORDPRESS_ADMIN_PASS
  WORDPRESS_ADMIN_EMAIL
)

# cleanup ENV variables
for e in "${ENVS[@]}"; do
  unset "$e"
done

apache2-foreground
