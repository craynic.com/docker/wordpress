#!/usr/bin/env bash

set -Eeuo pipefail

openssl req -config "/etc/ssl/openssl-localhost.cnf" -x509 -nodes -days 3650 -newkey rsa:4096 -extensions v3_req \
  -keyout "/etc/ssl/localhost.key" \
  -out "/etc/ssl/localhost.pem"