#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$MAIL_RELAYHOST" ]]; then
  echo "Mail configuration not provided, running without mail support."
  exit 0
fi

# check for mandatory ENV variables
CONFIG_FAIL=0

if [[ -z "$MAIL_RELAYHOST" ]]; then
  echo "Please define the relay host in the MAIL_RELAYHOST environment variable." >/dev/stderr
  CONFIG_FAIL=1
fi

if [[ -n "$MAIL_TLS_KEYFILE" || -n "$MAIL_TLS_CRTFILE" ]]; then
  if [[ -z "$MAIL_TLS_KEYFILE" || -z "$MAIL_TLS_CRTFILE" ]]; then
    echo "Please define both MAIL_TLS_KEYFILE and MAIL_TLS_CRTFILE." >/dev/stderr
    CONFIG_FAIL=1
  fi
fi

if [[ $CONFIG_FAIL -gt 0 ]]; then
  exit 1
fi

MSMTPRC_PATH="/etc/msmtprc"

# update the MSMTP configuration file with the mandatory ENV variables
cat <<EOF >"$MSMTPRC_PATH"
account default
host $MAIL_RELAYHOST
tls on
tls_starttls on
EOF

# if TLS client authentication key is provided, update the configuration
if [[ -n "$MAIL_TLS_KEYFILE" || -n "$MAIL_TLS_CRTFILE" || -n "$MAIL_TLS_CAFILE" ]]; then
  if [[ -n "$MAIL_TLS_KEYFILE" || -n "$MAIL_TLS_CRTFILE" ]]; then
    cat <<EOF >>"$MSMTPRC_PATH"
tls_key_file $MAIL_TLS_KEYFILE
tls_cert_file $MAIL_TLS_CRTFILE
EOF
  fi

  if [[ -n "$MAIL_TLS_CAFILE" ]]; then
    cat <<EOF >>"$MSMTPRC_PATH"
tls_certcheck on
tls_trust_file $MAIL_TLS_CAFILE
EOF
  else
    cat <<EOF >>"$MSMTPRC_PATH"
tls_certcheck off
EOF
  fi
fi
