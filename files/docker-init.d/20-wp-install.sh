#!/usr/bin/env bash

WPCMD="wp --allow-root"

if $WPCMD core is-installed; then
  echo "Wordpress is already installed."
else
  echo "Wordpress is not installed, installing..."
  $WPCMD core install \
    --path="/var/www/html" \
    --url="$WORDPRESS_URL" \
    --title="$WORDPRESS_TITLE" \
    --admin_user="$WORDPRESS_ADMIN_NAME" \
    --admin_password="$WORDPRESS_ADMIN_PASS" \
    --admin_email="$WORDPRESS_ADMIN_EMAIL" \
    --skip-email

  # update themes
  echo "Updating themes..."
  $WPCMD theme update --all

  echo "Installation finished."
fi
