# Wordpress

Enhanced Wordpress image with automated installation & HTTPS support.

## Environment variables

On top of the ENV variables supported by the
[base image](https://hub.docker.com/_/wordpress/), this
container requires some additional ENV variables.

### Wordpress installation

This image installs Wordpress (if not already installed)
on the container start. To do so, it requires these attributes:

* `WORDPRESS_URL`: URL of this Wordpress instance
* `WORDPRESS_TITLE`: title of this Wordpress instance
* `WORDPRESS_ADMIN_NAME`: user name of the administrator
* `WORDPRESS_ADMIN_PASS`: password of the administrator
* `WORDPRESS_ADMIN_EMAIL`: e-mail address of the administrator

### Mail support

To enable e-mail support out-of-the-box, please specify
the following ENV variables:

* `MAIL_RELAYHOST`: relay host for e-mail support; if not set,
    mail support is disabled.
* `MAIL_TLS_KEYFILE`: Path to a file holding the private key for
    client authentication. Optional; if specified,
    `MAIL_TLS_CRTFILE` must be set as well.
* `MAIL_TLS_CRTFILE`: Path to a file holding the public certificate
    for client authentication. Optional; if specified,
    `MAIL_TLS_KEYFILE` must be set as well.
* `MAIL_TLS_CAFILE`: Path to a file holding the CA certificate
    for server verification.

## Local Development

### With `docker-compose`

Run
```shell script
docker-compose up
```

### Without `docker-compose`

First, create the database:

```shell script
docker run -dit -p 3306:3306 \
  -e MYSQL_RANDOM_ROOT_PASSWORD="yes" \
  -e MYSQL_DATABASE="wordpress" \
  -e MYSQL_USER="wordpress" \
  -e MYSQL_PASSWORD="wordpress" \
  --rm --name "wordpress-mariadb" \
  "mariadb"
```

Then build the Wordpress custom container:

```shell script
docker build . -t wordpress-craynic
```

And finally run the container:

```shell script
docker run -dit -p 80:80 -p 443:443 \
  --rm --name "wordpress-craynic" \
  -e WORDPRESS_DB_HOST="172.17.0.1" \
  -e WORDPRESS_DB_USER="wordpress" \
  -e WORDPRESS_DB_PASSWORD="wordpress" \
  -e WORDPRESS_DB_NAME="wordpress" \
  -e WORDPRESS_DB_COLLATE="utf8_general_ci" \
  -e WORDPRESS_URL="localhost" \
  -e WORDPRESS_TITLE="Yet Another Wordpress Blog" \
  -e WORDPRESS_ADMIN_NAME="admin" \
  -e WORDPRESS_ADMIN_PASS="admin" \
  -e WORDPRESS_ADMIN_EMAIL="admin@example.com" \
  "wordpress-craynic"
```
