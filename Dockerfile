FROM library/wordpress:5.4-php7.4

ARG DEBIAN_FRONTEND=noninteractive

COPY files/ /

RUN apt-get update \
    # install msmtp-mta
    && apt-get install -y --no-install-recommends msmtp-mta=1.* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    # install wp-cli
    && curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp \
    # enable our custom configuration
    && a2ensite 001-site-ssl \
    && a2enmod ssl \
    && a2enconf craynic

EXPOSE 443

ENV WORDPRESS_URL="" \
    WORDPRESS_TITLE="" \
    WORDPRESS_ADMIN_NAME="" \
    WORDPRESS_ADMIN_PASS="" \
    WORDPRESS_ADMIN_EMAIL="" \
    MAIL_RELAYHOST="" \
    MAIL_TLS_KEYFILE="" \
    MAIL_TLS_CRTFILE="" \
    MAIL_TLS_CAFILE=""

CMD ["apache2-craynic.sh"]